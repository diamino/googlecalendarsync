# GoogleCalendarSync #

GoogleCalendarSync is a library that will sync your appointments in a Google Calendar to a structure in Python (a so called DAO). With this it is easy to write a program or script that will act on events in your calendar. The DAO is periodically synchronized to account for appointments that are added, removed or rescheduled.

# Installation of the library #

## The easy more stable way (pip) ##

Installation using `pip` will automatically install the required dependencies.

```
pip install googlecalendarsync
```

## The harder but fresher way (git clone) ##

The git repository is the place where new developments are stored. To have the latest (but possibly less stable) version of the library the git version is the one you want.

Clone the repository and install the library:
```
git clone https://bitbucket.org/diamino/googlecalendarsync.git
cd googlecalendarsync
python setup.py install
```

# Configuration #

## 'Developer' part ##

For the library to function (or more precise: to be able to request a client token) a developer API key for the Google Calendar API is needed. A key can be obtained via the [Google API console pages](https://console.developers.google.com/apis/api/calendar/overview). Create a project and enable the Calendar API. In the credentials tab you can download the API secrets by pressing the *Download JSON* button. Make sure the file is named `client_secrets.json`.

## User part ##

To access a specific Google Calendar an OAuth token needs to be obtained. For this a simple script is available in the library folder: `request_token.py`. It is a modified version of the default Calendar API example script. Copy the script in a folder of choice and make sure to set the `CALENDAR` constant in the file to the Google Calendar ID of the calendar you want to sync. You can find the Google Calendar ID (something like *s0merand0m5tr1ng@group.calendar.google.com*) in the settings of the Google Calendar website.

Now execute the script:
```
python request_token.py
```

If everything goes well a browserwindow will be opened and a Google Sign-in page will be loaded. Sign-in with Google account that is associated with the calendar you want to access. When signed-in a page is shown that requests the service configured under the ['Developer' part](#developer-part) to have (read-only) access to the specified calendar. When the access is granted the script will exit and a file called `sample.gtoken` should have been created. This file can be used to access the calendar using the GoogleCalendarSync library.

# Usage #

Make sure you have a token to access a specific Google calendar (see [Configuration](#configuration)).

See the `googlecalendartest.py` script in the library folder for an example of the usage of the GoogleCalendarSync library.

Main parts are:

## Import module ##
```
import googlecalendarsync # The main library
```

## Prepare some constants ##
```
GOOGLECALENDARID = 's0merand0m5tr1ng@group.calendar.google.com' # The Google Calendar ID
CREDFILE = 'sample.gtoken' # The file where the token for the calendar is stored
GOOGLEINTERVAL = 30 # Interval in seconds between sync actions
```

## Instantiate DAO ##
```
gedao = googlecalendarsync.GoogleEventDao()
```

## Instantiate GoogleCalendarSync object ##
```
gcs = googlecalendarsync.GoogleCalendarSync(gedao, GOOGLECALENDARID, interval=GOOGLEINTERVAL)
gcs.authorize(CREDFILE)
```

The GoogleCalendarSync routine is not running in a seperate thread by itself. Probably you want to wrap it in a thread.
```
gcsThread = threading.Thread(target=gcs.run, name="gcsThread")
gcsThread.daemon = True  # Make sure the thread stops when the script exits
gcsThread.start()
```

## Access the DAO ##

Access to the DAO should be thread safe so you can access the events in the DAO with the `peekEvent()` and `popEvent()` methods.
